<?php

namespace App\DataFixtures;

use App\Entity\UserApi;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class UserApiFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new UserApi();
        $userAdmin->setEmail('apiUser@admin.com');
        $userAdmin->setPassword('$2y$12$wiN7w.K4bj9HWa/qwtLUb.EPvzgMyXgJpd9Ar4yw1rJDz7G9Lvvny'); #admin
        $userAdmin->setRoles(['ROLE_API']);
        $userAdmin->setApiToken('eb0bcb7b7b9c1f4636538663f74ee7a6');
        // $userAdmin->setIsVerified(true);

        $manager->persist($userAdmin);
        $manager->flush();
        $manager->flush();
    }
}
